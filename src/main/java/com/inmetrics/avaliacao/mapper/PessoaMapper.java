package com.inmetrics.avaliacao.mapper;


import com.inmetrics.avaliacao.dto.PessoaDto;
import com.inmetrics.avaliacao.model.Pessoa;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PessoaMapper {

    PessoaMapper INSTANCE = Mappers.getMapper(PessoaMapper.class);

    PessoaDto pessoaParaDto(Pessoa p);

    List<PessoaDto> listaPessoaParaDto(List<Pessoa> lista);

}
