package com.inmetrics.avaliacao.dto;

import com.inmetrics.avaliacao.model.Telefone;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PessoaDto {

    @ApiModelProperty(example = "João da Silva")
    private String nome;
    private List<Telefone> telefone;
}
