package com.inmetrics.avaliacao.repository;

import com.inmetrics.avaliacao.model.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

    List<Pessoa> findAll();

    @Query(value = "select * from pessoa where nome = ?1", nativeQuery = true)
    public Pessoa buscarPessoaPorNome(@Param("nome") String nome);

    @Query(value = "select p from Pessoa p where id_pessoa = :id")
    public Pessoa buscarPessoaPorId(@Param("id") Long id);
}
