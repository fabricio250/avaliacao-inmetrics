package com.inmetrics.avaliacao.repository;

import com.inmetrics.avaliacao.exception.RegistroNaoEncontradoException;
import com.inmetrics.avaliacao.exception.ErroInternoException;
import com.inmetrics.avaliacao.model.Cep;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Repository
public class ConsultaCepRepository {

//    public Cep consultaCep(String uri) {
//
//        Cep cep;
//
//        try {
//            RestTemplate template = new RestTemplateBuilder()
//                    .rootUri(uri)
//                    .build();
//
//            cep = template.getForObject("/", Cep.class);
//            if(cep.getCep() == null){
//                throw new RegistroNaoEncontradoException("Sem resultados para a pesquisa");
//            }
//        } catch (HttpClientErrorException e){
//            throw new ErroInternoException("Erro interno");
//        }
//
//        return cep;
//
//    }

    public Cep consultaCep(String uri) {
        Cep cep;
        RestTemplate template = new RestTemplateBuilder()
                .rootUri(uri)
                .build();

        try{
            cep = template.getForObject("/", Cep.class);
        }catch (HttpClientErrorException e){
            throw new ErroInternoException("Erro interno");
        }

        return cep;

    }
}
