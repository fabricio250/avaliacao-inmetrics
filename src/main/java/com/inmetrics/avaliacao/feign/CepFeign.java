package com.inmetrics.avaliacao.feign;

import com.inmetrics.avaliacao.model.Cep;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "CepFeign", url = "https://viacep.com.br")
public interface CepFeign {


    @RequestMapping("/ws/{cep}/json/")
    Cep buscaCep(@PathVariable("cep") String cep);
}