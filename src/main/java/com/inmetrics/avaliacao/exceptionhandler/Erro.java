package com.inmetrics.avaliacao.exceptionhandler;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@ApiModel("Erro")
@Getter
@Builder
public class Erro {
    @ApiModelProperty(example = "0")
    private String codigo;
    @ApiModelProperty(example = "Sem resultados para a pesquisa")
    private String mensagem;
}
