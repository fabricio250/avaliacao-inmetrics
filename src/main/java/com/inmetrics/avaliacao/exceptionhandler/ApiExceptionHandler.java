package com.inmetrics.avaliacao.exceptionhandler;

import com.inmetrics.avaliacao.exception.RegistroNaoEncontradoException;
import com.inmetrics.avaliacao.exception.ErroInternoException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(RegistroNaoEncontradoException.class)
    public ResponseEntity<?> tratarRegistroNaoEncontradoException(RegistroNaoEncontradoException e){
        Erro erro = Erro.builder()
                .codigo("0")
                .mensagem(e.getMessage())
                .build();

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(erro);
    }

    @ExceptionHandler(ErroInternoException.class)
    public ResponseEntity<?> tratarCepNaoEncontradoException(ErroInternoException e){
        Erro erro = Erro.builder()
                .codigo("1")
                .mensagem(e.getMessage())
                .build();

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(erro);
    }
}
