package com.inmetrics.avaliacao.swagger;

import com.fasterxml.classmate.TypeResolver;
import com.inmetrics.avaliacao.exceptionhandler.Erro;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SpringFoxConfig implements WebMvcConfigurer {

    @Bean
    public Docket apiDocket(){
        var typeResolver = new TypeResolver();
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.inmetrics.avaliacao"))
                .build()
                .useDefaultResponseMessages(false)
                .additionalModels(typeResolver.resolve(Erro.class))
                .apiInfo(apiInfo())
                .tags(new Tag("CEP","Consulta de CEP"), new Tag("Pessoa",
                        "Consulta na base H2"));
    }

    private ApiInfo apiInfo(){
        return new ApiInfoBuilder()
                .title("Avaliação Rest Inmetrics")
                .version("1")
                .contact(new Contact("Fabrício Moreira","","fabricio.moreira@inmetrics.com.br"))
                .build();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}
