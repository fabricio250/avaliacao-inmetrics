package com.inmetrics.avaliacao.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class Cep {
    @ApiModelProperty(value = "Número do CEP", example = "40295140")
    private String cep;
    @ApiModelProperty(value = "Nome da rua/avenida", example = "Avenida Juracy Magalhães Júnior")
    private String logradouro;
    @ApiModelProperty(value = "Complemeto do logradouro", example = "Casa 2")
    private String complemento;
    @ApiModelProperty(value = "Nome do bairro", example = "Horto Florestal")
    private String bairro;
    @ApiModelProperty(value = "Nome da cidade", example = "Salvador")
    private String localidade;
    @ApiModelProperty(value = "Nome do estado", example = "Bahia")
    private String uf;
    @ApiModelProperty(value = "Código do IBGE", example = "2927408")
    private String ibge;
    @ApiModelProperty(value = "Guia de Informação e Apuração do ICMS", example = "1004")
    private String gia;
    @ApiModelProperty(value = "Código de telefone", example = "71")
    private String ddd;
    @ApiModelProperty(value = "Sistema Integrado de Administração Financeira", example = "7107")
    private String siafi;
}
