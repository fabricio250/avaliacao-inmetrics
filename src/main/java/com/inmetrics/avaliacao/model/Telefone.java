package com.inmetrics.avaliacao.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.persistence.*;


@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class Telefone {

    @ApiModelProperty(value = "ID",example = "1")
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_TELEFONE")
    private long id_telefone;

    @ApiModelProperty(example = "11111111111")
    @Column(name = "TELEFONE")
    private String telefone;

    @ManyToOne
    @JoinColumn(name = "ID_PESSOA")
    private Pessoa pessoa;

    @JsonBackReference
    public Pessoa getPessoa() {
        return pessoa;
    }
}
