package com.inmetrics.avaliacao.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class IsAlive {
    @ApiModelProperty(value = "Data atual", example = "03/12/2020")
    private String data;
    @ApiModelProperty(value = "Hora atual", example = "17:22")
    private String hora;

}
