package com.inmetrics.avaliacao.controller.swagger;

import com.inmetrics.avaliacao.dto.PessoaDto;
import com.inmetrics.avaliacao.exceptionhandler.Erro;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;

import java.util.List;

@Api(tags = "Pessoa")
public interface PessoaControllerSwagger {
    @ApiOperation("Lista todas as pessoas")
    public ResponseEntity<List<PessoaDto>> buscarPessoas();

    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Sem resultados para a pesquisa",response = Erro.class),
    })
    @ApiOperation("Busca pessoa por nome")
    public ResponseEntity<PessoaDto> buscarPessoaPorNome(@ApiParam(value = "Nome da pessoa",example = "xxx") String nome);

    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Sem resultados para a pesquisa",response = Erro.class),
    })
    @ApiOperation("Busca pessoa por ID")
    public ResponseEntity<PessoaDto> buscarPessoaPorId(@ApiParam(value = "ID da pessoa",example = "1") Long id);
}
