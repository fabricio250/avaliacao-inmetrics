package com.inmetrics.avaliacao.controller.swagger;

import com.inmetrics.avaliacao.exceptionhandler.Erro;
import com.inmetrics.avaliacao.model.Cep;
import com.inmetrics.avaliacao.model.IsAlive;
import io.swagger.annotations.*;

@Api(tags = "CEP")
public interface CepControllerSwagger {

    @ApiOperation("Retorna data e hora")
    public IsAlive isAlive();

    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Sem resultados para a pesquisa",response = Erro.class),
            @ApiResponse(code = 500, message = "Erro interno",response = Erro.class)
    })
    @ApiOperation("Consulta CEP via RestTemplate")
    public Cep consultaCepRestTemplate(@ApiParam(value = "CEP a consultar",example = "11111111") String cep);

    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Sem resultados para a pesquisa",response = Erro.class),
            @ApiResponse(code = 500, message = "Erro interno",response = Erro.class)
    })
    @ApiOperation("Consulta CEP via Feign Client")
    public Cep consultaCepFeign(@ApiParam(value = "CEP a consultar",example = "11111111") String cep);

}
