package com.inmetrics.avaliacao.controller;

import com.inmetrics.avaliacao.controller.swagger.CepControllerSwagger;
import com.inmetrics.avaliacao.model.Cep;
import com.inmetrics.avaliacao.model.IsAlive;
import com.inmetrics.avaliacao.service.CepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class CepController implements CepControllerSwagger {

    @Autowired
    CepService cepService;

    @GetMapping(path = "/isAlive", produces = MediaType.APPLICATION_JSON_VALUE)
    public IsAlive isAlive(){
        return cepService.isAlive();
    }

    @GetMapping(path = "/cep/cepRestTemplate/{cep}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Cep consultaCepRestTemplate(@PathVariable("cep") String cep){

            return cepService.consultaCepRestTemplate(cep);

    }

    @GetMapping(path = "/cep/cepFeing/{cep}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Cep consultaCepFeign(@PathVariable("cep") String cep){

        return cepService.consultaCepFeign(cep);
    }
}
