package com.inmetrics.avaliacao.controller;

import com.inmetrics.avaliacao.controller.swagger.PessoaControllerSwagger;
import com.inmetrics.avaliacao.dto.PessoaDto;
import com.inmetrics.avaliacao.service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class PessoaController implements PessoaControllerSwagger {

    @Autowired
    PessoaService pessoaService;

    @GetMapping(path = "/pessoa/pessoas", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PessoaDto>> buscarPessoas(){
        return new ResponseEntity<>(pessoaService.buscarPessoas(),HttpStatus.OK);
    }

    @GetMapping(path = "/pessoa/pessoasNome/{nome}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PessoaDto> buscarPessoaPorNome(@PathVariable("nome") String nome){
        return new ResponseEntity<>(pessoaService.buscarPessoaPorNome(nome),HttpStatus.OK);
    }

    @GetMapping(path = "/pessoa/pessoasId/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PessoaDto> buscarPessoaPorId(@PathVariable("id") Long id){
        return new ResponseEntity<>(pessoaService.buscarPessoaPorId(id), HttpStatus.OK);
    }
}
