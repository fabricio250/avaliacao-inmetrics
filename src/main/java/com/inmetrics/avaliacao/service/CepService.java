package com.inmetrics.avaliacao.service;

import com.inmetrics.avaliacao.feign.CepFeign;
import com.inmetrics.avaliacao.model.IsAlive;
import com.inmetrics.avaliacao.exception.RegistroNaoEncontradoException;
import com.inmetrics.avaliacao.exception.ErroInternoException;
import com.inmetrics.avaliacao.repository.ConsultaCepRepository;
import com.inmetrics.avaliacao.model.Cep;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class CepService {

    @Autowired
    ConsultaCepRepository consultaCepRepository;

    @Autowired
    private CepFeign cepFeign;

    public IsAlive isAlive(){
        DateFormat dataFormatada = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat horaFormatada = new SimpleDateFormat("HH:mm");

        return new IsAlive(dataFormatada.format(new Date()),horaFormatada.format(new Date()));
    }

    public Cep consultaCepRestTemplate(String cep) {
        Cep cep1 = consultaCepRepository.consultaCep("https://viacep.com.br/ws/" + cep +  "/json/");

        if(cep1.getLocalidade() == null){
            throw new RegistroNaoEncontradoException("Sem resultados para a pesquisa");
        }else{
            return cep1;
        }
    }

    public Cep consultaCepFeign(String cep){
        try {
            Cep cepRt = cepFeign.buscaCep(cep);
            if(cepRt.getCep() == null){
                throw new RegistroNaoEncontradoException("Sem resultados para a pesquisa");
            }
            return cepRt;
        }catch (FeignException e){
            throw new ErroInternoException("Erro interno");
        }
    }
}
