package com.inmetrics.avaliacao.service;

import com.inmetrics.avaliacao.dto.PessoaDto;
import com.inmetrics.avaliacao.exception.RegistroNaoEncontradoException;
import com.inmetrics.avaliacao.mapper.PessoaMapper;
import com.inmetrics.avaliacao.model.Pessoa;
import com.inmetrics.avaliacao.repository.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class PessoaService {

    @Autowired
    PessoaRepository pessoaRepository;


    private PessoaMapper pessoaMapper;


    public List<PessoaDto> buscarPessoas(){
        return pessoaMapper.INSTANCE.listaPessoaParaDto(pessoaRepository.findAll());
    }

    public PessoaDto buscarPessoaPorNome(String nome){

            Pessoa pessoa = pessoaRepository.buscarPessoaPorNome(nome);
            if(pessoa == null){
                throw new RegistroNaoEncontradoException("Sem resultados para a pesquisa");
            }


            return pessoaMapper.INSTANCE.pessoaParaDto(pessoa);

    }

    public PessoaDto buscarPessoaPorId(Long id){

        Pessoa pessoa = pessoaRepository.buscarPessoaPorId(id);
        if(pessoa == null){
            throw new RegistroNaoEncontradoException("Sem resultados para a pesquisa");
        }
        return pessoaMapper.INSTANCE.pessoaParaDto(pessoa);

    }
}
