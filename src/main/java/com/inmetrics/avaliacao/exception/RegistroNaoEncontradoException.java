package com.inmetrics.avaliacao.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegistroNaoEncontradoException extends RuntimeException{

    public RegistroNaoEncontradoException(String message) {
        super(message);
    }
}
