package com.inmetrics.avaliacao.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErroInternoException extends RuntimeException{

    public ErroInternoException(String message) {
        super(message);
    }
}
